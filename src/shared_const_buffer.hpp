#ifndef __shared_const_buffer_hpp__
#define __shared_const_buffer_hpp__

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>


// A reference-counted non-modifiable buffer class.
class shared_const_buffer
{
public:
	// Construct from a std::string.
	explicit shared_const_buffer(const std::string& data)
		: data_(new std::vector<char>(data.begin(), data.end())),
		buffer_(boost::asio::buffer(*data_))
	{
	}

	// Implement the Const_Buffers concept.
	typedef boost::asio::const_buffer value_type;
	typedef const boost::asio::const_buffer* const_iterator;
	const_iterator begin() const { return &buffer_; }
	const_iterator end() const { return &buffer_ + 1; }

private:
	boost::shared_ptr<std::vector<char> > data_;
	boost::asio::const_buffer buffer_;
};

#endif
