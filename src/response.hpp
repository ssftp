#ifndef __reply_hpp__
#define __reply_hpp__

#include <string>


namespace ssftp
{
	typedef char response_code_type;

	namespace response_codes
	{
		enum { success='+', error='-', logged_in='!' };
// 		const response_code_type success = '+';
// 		const response_code_type error = '-';
// 		const response_code_type logged_in = '!';
	}

	struct response
	{
		response_code_type code;
		std::string message;

		response()
			:code(' ')
		{}

		response(response_code_type code, std::string message)
			:code(code)
			,message(message)
		{}
	};

	struct success_response : response
	{
		success_response(std::string message)
			:response(response_codes::success, message) 
		{}
	};

	struct error_response : response
	{
		error_response(std::string message)
			:response(response_codes::error, message) 
		{}
	};

	struct no_response : response {};
}

#endif