#include "connection.hpp"
#include "shared_const_buffer.hpp"
#include <iostream>
#include <string>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost;


ssftp::connection::connection(asio::io_service& io_service)
	:socket(io_service)
{
	//TODO dumm in connection
	resp_callbacks.push(boost::bind(&connection::handle_greeting, this, _1));
}

void ssftp::connection::async_read_command(read_command_handler handler)
{
	asio::async_read_until(socket, data_, '\0',
		boost::bind(&connection::handle_read_until_command, this, _1, _2, handler));
}

void ssftp::connection::async_write_command(command command, response_handler callback)
{
	resp_callbacks.push(callback);

	string str = command.cmd											//<cmd>
				+ ((command.args.length()>0) ? (" " + command.args):"")	//<args>
				+ '\0';													//<NULL>
	asio::async_write(socket, shared_const_buffer(str), 
		boost::bind(&connection::handle_write_command, this, _1, _2));
}

void ssftp::connection::async_read_response(read_response_handler handler)
{
	asio::async_read_until(socket, data_, '\0',
		boost::bind(&connection::handle_read_until_response, this, _1, _2, handler));	
}

void ssftp::connection::async_write_response(response resp)
{
	string str = resp.code								//<response-code>
		+ (resp.message.length()>0 ? resp.message : "")	//<message>
		+ '\0';											//<NULL>
	asio::async_write(socket, shared_const_buffer(trim_copy(str)), 
		boost::bind(&connection::handle_write_response, this, _1, _2));
}

void ssftp::connection::handle_read_until_command(
	const boost::system::error_code& error, size_t, read_command_handler handler)
{
	if(error)
	{
		disconnected_signal();
	}
	else
	{
		command command;
		istream is(&data_);

		is >> command.cmd;
		is.get();	//Leerzeichen zwischen <cmd> und <args>
		if(is.good())
			getline(is, command.args, '\0');
		else
			trim_if(command.cmd, is_cntrl());	//Wenn keine args, dann haengt n '\0' am cmd -> wegtrimmen

		//Restliches Zeug lesen (duerfte nur ein '\0' sein..
		while (is.good() && is.get());

		handler(command);
	}
}

void ssftp::connection::handle_read_until_response(
	const boost::system::error_code& error, size_t, read_response_handler handler)
{
	if(error)
	{
		disconnected_signal();
	}
	else
	{
		response resp;
		istream is(&data_);

		resp.code = is.get();	//<response-code>
		getline(is, resp.message);	//message

		//Restliches Zeug lesen
		while (is.good() && is.get());

		resp_callbacks.front()(resp);
		resp_callbacks.pop();
		handler(resp);
	}
}

void ssftp::connection::handle_write_response(const boost::system::error_code& error, size_t bytes)
{
	if(error)
		disconnected_signal();
	else
		clog << "response written to socket. (" << bytes << " bytes sent)" << endl;
}

void ssftp::connection::handle_write_command(const boost::system::error_code& error, size_t bytes)
{
	if(error)
		disconnected_signal();
	else
		clog << "command written to socket. (" << bytes << " bytes sent)" << endl;
}

void ssftp::connection::on_disconnected(void_handler handler)
{
	disconnected_signal.connect(handler);
}