#ifndef __exception_hpp__
#define __exception_hpp__

#include <exception>


namespace ssftp
{
	namespace server
	{
		class done_exception : public std::exception {};
	}
}

#endif