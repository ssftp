#ifndef __server_hpp__
#define __server_hpp__

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <map>
#include <string>
#include "file_system_helper.hpp"


namespace ssftp
{
	namespace server
	{
		class session;

		class server
		{
			boost::asio::io_service io_service;
			boost::asio::io_service::work work;
			boost::asio::ip::tcp::acceptor acceptor;

			std::vector<boost::shared_ptr<session> > sessions;

		protected:
			//Fuer reverse_server, damit acceptor nicht gebunden wird
			server()
				:work(io_service)
				,acceptor(io_service)
			{}

		public:
			std::map<std::string, std::string> usermap;
			file_system_helper::directory_list root_dirs;

			server(unsigned short port);

			boost::asio::io_service& get_io_service()
			{
				return this->io_service;
			}
			
			void run();
		};
	}
}

#endif
