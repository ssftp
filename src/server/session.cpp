#include "session.hpp"
#include "../response.hpp"
#include "../exception.hpp"
#include <boost/bind.hpp>
#include <iostream>


using namespace std;
using namespace boost;


ssftp::server::session::session(ssftp::server::server& server)
	:session_base(server.get_io_service())
	,server(server)
	,file_sys_helper(server.root_dirs)
	,cmd_processor(*this)
{
}

void ssftp::server::session::start()
{
	conn.async_write_response(success_response(asio::ip::host_name()+" SFTP Server Service"));

	conn.async_read_command(boost::bind(&session::handle_read_command, this, _1));
}

asio::ip::tcp::socket& ssftp::server::session::socket()
{
	return conn.socket;
}

void ssftp::server::session::handle_read_command(command cmd)
{
	process_command(cmd);
}

void ssftp::server::session::process_command(command command)
{
	clog << command.cmd << " " << command.args << endl;

	try
	{
		response resp = cmd_processor.process(command);
		if(typeid(resp) != typeid(no_response))
			conn.async_write_response(resp);
	}
	catch(done_exception&)
	{
		conn.socket.close();
	}

	conn.async_read_command(boost::bind(&session::handle_read_command, this, _1));
}