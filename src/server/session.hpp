#ifndef __session_hpp__
#define __session_hpp__

#include <boost/asio.hpp>
#include <boost/function.hpp>
#include "../connection.hpp"
#include "../command.hpp"
#include "../session_base.hpp"
#include "command_processor.hpp"
#include "file_system_helper.hpp"
#include "server.hpp"


namespace ssftp
{
	typedef boost::function<void()> void_handler_type;

	namespace server
	{		
		class session : public session_base
		{
			friend class command_processor;

			server& server;
			file_system_helper file_sys_helper;
			command_processor cmd_processor;

			void handle_read_command(command cmd);

			void process_command(command cmd);

		public:
			typedef boost::shared_ptr<session> ptr;

			/** Konstruktor.
			*	@param server Der zur Session gehoerige Server.
			*/
			session(ssftp::server::server& server);

			virtual void start();

			boost::asio::ip::tcp::socket& socket();
		};
	} 
}

#endif