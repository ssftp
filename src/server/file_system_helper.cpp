#include "file_system_helper.hpp"
#include <boost/lambda/lambda.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <algorithm>

using namespace std;
using namespace boost;


ssftp::server::file_system_helper::file_system_helper(directory_list root_dirs)
	:root_dirs(root_dirs)
	,current_dir("<root>")
{
}

string ssftp::server::file_system_helper::current_directory() const
{
	return current_dir.string();
}

bool ssftp::server::file_system_helper::change_directory(string dir_str)
{
	filesystem::path param_dir(dir_str);
	filesystem::path new_dir;

	if(param_dir.leaf() != "/")
		param_dir /= "/";

	// absoluter Pfad
	if(param_dir.is_complete())
		new_dir = param_dir.normalize();
	// relativer pfad
	else
		new_dir = current_dir / param_dir;

	if(filesystem::exists(new_dir))
		current_dir = new_dir.normalize();
	else
		return false;

	std::string rel_path = current_dir.relative_path().string();
	//Wenn relativer Pfad nur aus '.' und '/' besteht, dann auf <root> setzen
	if(!rel_path.empty() && all(rel_path, is_any_of("./")))
		current_dir = "<root>";

	return true;
}

ssftp::server::file_system_helper::file_list 
ssftp::server::file_system_helper::get_file_list()
{
	//In <root> gibts keine Dateien..
	if(current_dir=="<root>")
	{
		return directory_list(0);
	}

	directory_list ret;


	return ret;
}

ssftp::server::file_system_helper::directory_list 
ssftp::server::file_system_helper::get_directory_list()
{
	//Bei <root>-Verz. eine Liste den Root-Verz.
	if(current_dir=="<root>")
	{
		return root_dirs;
	}

	directory_list ret;
	filesystem::directory_iterator end_iter;
	// currentDir iterieren
	for(filesystem::directory_iterator dir_iter(current_dir);
		dir_iter != end_iter; ++dir_iter)
	{
// 		try
// 		{
			if(filesystem::is_directory(dir_iter->status()))
				ret.push_back(dir_iter->leaf());;
// 		}
// 		catch(const exception& ex)
// 		{
// 			Command cmd(CoreModule::ErrorCmd);
// 			cmd.AddParam("FileSystemModule::ListenDirectory: Fehler: " + string(ex.what()));
// 			this->CommandSocket()->Send(cmd);
// 			return;
// 		}
	}

	return ret;
}

bool ssftp::server::file_system_helper::delete_file(string file)
{
	try
	{
		return filesystem::remove(current_dir/file);
	}
	catch(filesystem::filesystem_error&)
	{
		return false;
	}
}

bool ssftp::server::file_system_helper::exists(string file)
{
	if(filesystem::path(file).is_complete())
		return filesystem::exists(file);
	else
		return filesystem::exists(current_dir/file);
}

bool ssftp::server::file_system_helper::rename(string old_filename, string new_filename)
{
	filesystem::path from(complete(old_filename)),
					to(complete(new_filename));

// 	if(from.leaf() != "/")
// 		from /= "/";
// 	if(to.leaf() != "/")
// 		from /= "/";

// 	if(false == from.is_complete())
// 		from = current_dir / from;
// 	if(false == to.is_complete())
// 		to = current_dir / to;

	if(filesystem::exists(to))
		return false;

	filesystem::rename(from, to);
	return true;
}

string ssftp::server::file_system_helper::complete(string filename)
{
	filesystem::path path(filename);

// 	if(path.leaf() != "/")
// 		path /= "/";

	if(path.is_complete())
		return path.string();
	else
		return (current_dir/path).string();
}

std::size_t ssftp::server::file_system_helper::file_size(string filename)
{
	return filesystem::file_size(complete(filename));
}