#include "command_processor.hpp"
#include "session.hpp"
#include "../exception.hpp"
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH
#include <iostream>
#include <fstream>

using namespace std;
using namespace boost;
using namespace boost::asio::ip;


ssftp::server::command_processor::command_processor(ssftp::server::session& session)
	:session(session)
	,file_sys_helper(session.file_sys_helper)
	,login_state(login_states::none)
	,rename_state(rename_states::none)
	,retr_state(retr_states::none)
{
	command_fun_map["user"] = boost::bind(&command_processor::user, this, _1);
	command_fun_map["pass"] = boost::bind(&command_processor::pass, this, _1);
	command_fun_map["list"] = boost::bind(&command_processor::list, this, _1);
	command_fun_map["cdir"] = boost::bind(&command_processor::cdir, this, _1);
	command_fun_map["kill"] = boost::bind(&command_processor::kill, this, _1);
	command_fun_map["name"] = boost::bind(&command_processor::name, this, _1);
	command_fun_map["tobe"] = boost::bind(&command_processor::tobe, this, _1);
	command_fun_map["done"] = boost::bind(&command_processor::done, this, _1);
	command_fun_map["retr"] = boost::bind(&command_processor::retr, this, _1);
	command_fun_map["send"] = boost::bind(&command_processor::send, this, _1);
	command_fun_map["stop"] = boost::bind(&command_processor::stop, this, _1);
}

/**	Behandelt einen command und gibt den reply zurueck.
*/
ssftp::response ssftp::server::command_processor::process(command command)
{
	to_lower(command.cmd);
	if(command_fun_map.find(command.cmd) == command_fun_map.end())
	{
		cerr<< "command_processor.process: Command '" 
			<< command.cmd 
			<< "' ist unbekannt (keine Handlerfunktion definiert)." << endl;
		return error_response("Command not implemented.");
	}
	else
	{
		return command_fun_map[command.cmd](command.args);
	}
}

/**	USER
*/
ssftp::response ssftp::server::command_processor::user(string username)
{
	//Wenn Laenge==0 oder irgendein Zeichen ein Spacezeichen
	if(username.length()==0 || !all(username, !is_space()))
		return error_response("Invalid argument");

	if(login_state == login_states::logged_in)
	{
		return error_response("You are already logged in");
	}

	//Wenn es den benutzer nicht gibt
	if(session.server.usermap.find(username) == session.server.usermap.end())
	{
		return error_response("Invalid user-id");
	}
	//sonst Usernamen merken und Passwort anfordern
	else
	{
		this->username = username;
		login_state = login_states::intermediate;
		return success_response("User-id valid, need password");;
	}
}

/**	PASS
*/
ssftp::response ssftp::server::command_processor::pass(string password)
{
	if(login_state == login_states::none)
	{
		return error_response("need user-id first");
	}
	else if(login_state == login_states::logged_in)
	{
		return error_response("already logged in");
	}
	else
	//Ist der Username dem Server bekannt und stimmen die Passwoerter ueberein?
	if(login_state == login_states::intermediate 
		&& session.server.usermap[username]==password)
	{
		login_state = login_states::logged_in;
		return response(response_codes::logged_in, "Logged in");
	}
	else
	{
		login_state = login_states::none;
		return error_response("Wrong password");
	}
}

/**	LIST
 *	Erste implementation erstmal nur mit F.
 */
ssftp::response ssftp::server::command_processor::list(string args)
{
	success_response ret(file_sys_helper.current_directory()); 

	foreach(string dir, file_sys_helper.get_directory_list())
	{
		ret.message += "\r\n" + dir;
	}
	foreach(string file, file_sys_helper.get_file_list())
	{
		ret.message += "\r\n" + file;
	}

	return ret;
}

/**	CDIR
 */	
ssftp::response ssftp::server::command_processor::cdir(string dir_str)
{
	if(dir_str.empty())
		return error_response("Syntax error, need parameter: CDIR <new-directory>");
	if(file_sys_helper.change_directory(dir_str))
		return response('!', "Changed working dir to " + file_sys_helper.current_directory());
	else
		return error_response("Can't connect to directory");
}

/**	KILL
 */	
ssftp::response ssftp::server::command_processor::kill(string file)
{
	if(file_sys_helper.delete_file(file))
		return success_response(file+" deleted");
	else
		return error_response("Not deleted");
}

/**	NAME
*/	
ssftp::response ssftp::server::command_processor::name(string old_filename)
{
	if(file_sys_helper.exists(old_filename))
	{
		this->old_filename = file_sys_helper.complete(old_filename);
		rename_state = rename_states::intermediate;
		return success_response("File exists");
	}
	else
		return error_response("Can't find "+old_filename);
}

/**	TOBE
*/	
ssftp::response ssftp::server::command_processor::tobe(string new_filename)
{
	if(rename_state == rename_states::none)
		return error_response("File wasn't renamed because no file to rename was specified"); 
	else
	{
		rename_state = rename_states::none;
		if(file_sys_helper.rename(old_filename, new_filename))
			return success_response(old_filename+" renamed to "+new_filename);
		else
			return error_response("File wasn't renamed");
	}
}

/**	DONE
 */
ssftp::response ssftp::server::command_processor::done(string /*args*/)
{
	throw done_exception();
	//return success_response("this will never happen");
}

/**	RETR
 */
ssftp::response ssftp::server::command_processor::retr(string filename)
{
	if(false == file_sys_helper.exists(filename))
	{
		retr_state = retr_states::none;
		return error_response("File doesn't exist");
	}
	else
	{
		this->retr_filename = file_sys_helper.complete(filename);
		retr_state = retr_states::intermediate;
		return response(' ', boost::lexical_cast<string>(file_sys_helper.file_size(filename)));
	}
}

/**	SEND
 */
ssftp::response ssftp::server::command_processor::send(string /*args*/)
{
	//Wenn es vorher kein erfolgreiches RETR gab
	if(retr_state == retr_states::none)
	{
		return error_response("No File to send is being specified; do RETR first");
	}
	//Bei unbekannten Status
	else if(retr_state != retr_states::intermediate)
	{
		retr_state = retr_states::none;
		return error_response("Unknown internal state; try again");
	}
	//Sonst Datei senden
	else
	{
		char buf[4000];
		ifstream ifs(retr_filename.c_str(), ifstream::in|ifstream::binary);
		while(false == ifs.eof())
		{
			ifs.read(buf, sizeof(buf));
			asio::write(session.socket(), asio::buffer(buf, ifs.gcount()));
		}
		ifs.close();
		return no_response();
	}
}

/**	STOP
*/
ssftp::response ssftp::server::command_processor::stop(string /*args*/)
{
	return success_response("ok, RETR aborted");
}