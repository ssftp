#ifndef __file_system_helper_hpp__
#define __file_system_helper_hpp__

#include <list>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/noncopyable.hpp>


namespace ssftp
{
	namespace server
	{
		class file_system_helper : boost::noncopyable
		{
			typedef std::list<std::string> string_list;
		public:
			typedef string_list file_list;
			typedef string_list directory_list;

		private:
			const directory_list root_dirs;
			boost::filesystem::path current_dir;

		public:
			file_system_helper(directory_list root_dirs);

			std::string current_directory() const;

			bool change_directory(std::string dirStr);

			/**	Gibt eine Liste mit Dateien des aktuellen Verz. zurueck.
			 */
			file_list get_file_list();

			/**	Gibt eine Liste mit Ordnern des aktuellen Verz. zurueck.
			 */
			directory_list get_directory_list();

			/**	Loescht eine Datei _file_ im aktuellen Verz.
			 */
			bool delete_file(std::string file);

			bool exists(std::string file);

			/**	Benennt eine Datei um; liefert false, wenn es eine Datei 
			 *	mit dem neuen Dateinamen schon gibt.
			 */
			bool rename(std::string old_filename, std::string new_filename);

			/** Wenn ein relativer Pfad uebergeben wird, wird current_dir vorn angehaengt.
			 */
			std::string complete(std::string filename);

			std::size_t file_size(std::string filename);
		};
	}
}

#endif