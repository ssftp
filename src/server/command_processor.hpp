#ifndef __command_processor_hpp__
#define __command_processor_hpp__

#include <map>
#include <string>
#include <boost/function.hpp>
#include <boost/noncopyable.hpp>
#include "../response.hpp"
#include "../command.hpp"
#include "file_system_helper.hpp"


namespace ssftp
{
	namespace server
	{
		typedef int rename_state_type;
		namespace { namespace rename_states {
			enum { none, intermediate };
		}}

		typedef int login_state_type;
		namespace { namespace login_states {
			enum { none, intermediate, logged_in };
		}}

		typedef int retr_state_type;
		namespace { namespace retr_states {
			enum { none, intermediate };
		}}

		class session;

		class command_processor : boost::noncopyable
		{
			session& session;
			file_system_helper& file_sys_helper;
			std::map<std::string, boost::function<response(std::string)> > command_fun_map;

			login_state_type login_state;
			std::string username;

			rename_state_type rename_state;
			std::string old_filename;

			retr_state_type retr_state;
			std::string retr_filename;

			response user(std::string username);
			response pass(std::string password);
// 			response type(std::string args);
 			response list(std::string args);
 			response cdir(std::string dir);
			response kill(std::string filename);

 			response name(std::string old_filename);
			response tobe(std::string new_filename);

 			response done(std::string /*args*/);

			response retr(std::string filename);
			//Moegliche Befehle vom Client nach RETR:
			response send(std::string /*args*/);
			response stop(std::string /*args*/);

// 			response stor(std::string args);


		public:
			command_processor(ssftp::server::session& session);

			/**	Behandelt einen command und gibt den reply zurueck.
			*/
			response process(command command);
		};
	}
}

#endif