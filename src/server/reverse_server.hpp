#ifndef __reverse_server_hpp__
#define __reverse_server_hpp__

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <map>
#include <string>
#include "server.hpp"


namespace ssftp
{
	namespace server
	{
		class reverse_server : public server
		{
			static const unsigned short connect_interval_secs = 10;	//In Sekunden.
			boost::asio::deadline_timer timer;
			boost::asio::ip::tcp::endpoint remote_endpoint;			//Beinhaltet den Endpoint zum Connecten.

			boost::shared_ptr<session> session_ptr;

			void async_connect_to_client();
			void handle_connect(const boost::system::error_code& error, boost::shared_ptr<session> session_ptr);
			void handle_session_disconnected();

		public:
			reverse_server(const std::string& ip, unsigned short port);

			void run();
		};
	}
}

#endif