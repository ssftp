#include "reverse_server.hpp"
#include "session.hpp"
#include <iostream>
#include <boost/thread.hpp>

using namespace std;
using namespace boost;
using namespace boost::asio::ip;


ssftp::server::reverse_server::reverse_server(const std::string& ip, unsigned short port)
	:remote_endpoint(address::from_string(ip), port)
	,timer(get_io_service())
{
}

void ssftp::server::reverse_server::run()
{
	async_connect_to_client();
	get_io_service().run();
}

void ssftp::server::reverse_server::async_connect_to_client()
{
	clog << "Connecting.." << endl;
	session::ptr session_ptr(new session(*this));
	session_ptr->socket().async_connect(remote_endpoint,
		bind(&reverse_server::handle_connect, this, _1, session_ptr));
}

void ssftp::server::reverse_server::handle_connect(const boost::system::error_code& error, shared_ptr<session> session_ptr)
{
	if(!error)
	{
		clog << "Connection to Client etablished." << endl;
		this->session_ptr = session_ptr;
		session_ptr->on_disconnected(bind(&reverse_server::handle_session_disconnected, this));
		session_ptr->start();
	}
	else
	{
		clog << error.message() << " (Next attempt in " << connect_interval_secs << " seconds)";
		session_ptr.reset();
		timer.expires_from_now(posix_time::seconds(connect_interval_secs));
		timer.async_wait(bind(&reverse_server::async_connect_to_client, this));
	}
}

void ssftp::server::reverse_server::handle_session_disconnected()
{
	clog << "Client disconnected." << endl;
	session_ptr.reset();
	async_connect_to_client();
}