#include "server.hpp"
#include "session.hpp"
#include <iostream>
#include <boost/thread.hpp>

using namespace boost;
using namespace boost::asio::ip;


ssftp::server::server::server(unsigned short port)
	:work(io_service)
	,acceptor(io_service, tcp::endpoint(tcp::v4(), port))
{
}

void ssftp::server::server::run()
{
	thread thread(boost::bind(&boost::asio::io_service::run, &io_service));

	std::clog << "Server is now running - waiting for clients.." << std::endl;
	//clients annehmen
	for(;;)
	{
		//Neue Session erstellen
		session::ptr session_ptr(new session(*this));
		//Verbindung fuer den control socket annehmen
		acceptor.accept(session_ptr->socket());
		sessions.push_back(session_ptr);
		session_ptr->start();
		std::clog << "Client accepted." << std::endl;
	}
}