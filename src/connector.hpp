#ifndef connector_hpp_INCLUDED
#define connector_hpp_INCLUDED

#include <boost/asio.hpp>
#include <string>


namespace ssftp
{
// 	//free function
// 	boost::asio::ip::tcp::endpoint make_endpoint(const std::string& host, unsigned short port)
// 	{
// 		return boost::asio::ip::tcp::endpoint(boost::asio::ip::address(), port);
// 	}

	class connector
	{
		boost::asio::ip::tcp::socket& socket_;

	public:
		connector(boost::asio::ip::tcp::socket& _socket)
			:socket_(_socket)
		{}

		template<typename handler_type>
		void async_connect(const std::string& host, unsigned short port, handler_type handler)
		{
			using namespace boost::asio::ip;
			tcp::resolver resolver;
			tcp::resolver::query query(host, port);
			tcp::resolver::iterator iter = resolver.resolve(query);
			socket_.async_connect(*iter, handler);
		}
	};
}

#endif