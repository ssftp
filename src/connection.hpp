#ifndef __control_connection_hpp__
#define __control_connection_hpp__

#include <queue>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/signal.hpp>
#include "response.hpp"
#include "command.hpp"


namespace ssftp
{
	typedef boost::function<void()> void_handler;
	typedef boost::function<void(command)> read_command_handler;
	typedef boost::function<void(response)> read_response_handler;

	typedef boost::function<void(response)> response_handler;

	class connection
	{
		boost::asio::streambuf data_;
		std::queue<response_handler> resp_callbacks;
		boost::signal<void()> disconnected_signal;

		void handle_read_until_command(
			const boost::system::error_code& e, std::size_t, read_command_handler handler);
		void handle_read_until_response(
			const boost::system::error_code& e, std::size_t, read_response_handler handler);
		
		void handle_write_command(const boost::system::error_code& e, std::size_t bytes);

		void handle_write_response(const boost::system::error_code& e, std::size_t bytes);

		void handle_greeting(response resp)
		{
			//TODO auf +/- pruefen
		}

	public:
		boost::asio::ip::tcp::socket socket;

		connection(boost::asio::io_service& io_service);

		void on_disconnected(void_handler handler);

		void async_read_command(read_command_handler handler);
		/**	.
		 *	@param callback Funktion, die bei einer response auf den command aufgerufen wird.
		 */
		void async_write_command(command command, response_handler callback);

		void async_read_response(read_response_handler handler);
		void async_write_response(response resp);
	};
}

#endif