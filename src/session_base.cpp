#include "session_base.hpp"

using namespace boost;
using namespace boost::asio;


ssftp::session_base::session_base(io_service& io_service)
	://strand(io_service)
	conn(io_service)
{
}

void ssftp::session_base::on_disconnected(void_handler handler)
{
	conn.on_disconnected(handler);
}

asio::ip::tcp::socket& ssftp::session_base::socket()
{
	return conn.socket;
}

ssftp::connection& ssftp::session_base::connection()
{
	return conn;
}