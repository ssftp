#ifndef client_view_hpp_INCLUDED
#define client_view_hpp_INCLUDED

#include <SmartWin.h>
#include <boost/assign.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include "../client/client.hpp"
#include "../client/view.hpp"
#include "../client/session.hpp"


namespace ssftp
{
	namespace client
	{
		class client_view : view<client>, public SmartWin::WidgetFactory<SmartWin::WidgetWindow, client_view>
		{
			WidgetDataGridPtr sessions_data_grid;

			void handle_sized(const SmartWin::WidgetSizedEventResult&)
			{
				SmartWin::Place p;
				SmartWin::Rectangle r( getClientAreaSize() );
				p.setBoundsBorders( r, 4, 4 );

				sessions_data_grid->setBounds( r.left() );
				//currentDirButton->setBounds( r.right() );
			}

		public:
			typedef boost::shared_ptr<client_view> ptr;

			explicit client_view(client_ptr model_ptr)
				:view<client>(model_ptr)
			{
				createWindow();
				setText("Hello World");

				sessions_data_grid = createDataGrid();
				sessions_data_grid->createColumns( boost::assign::list_of("hallo")("lol") );

				this->onSized(&client_view::handle_sized);
				setBounds(100, 100, 200, 200);
			}

			virtual void refresh() const
			{
// 				sessions_data_grid->removeAllRows();
// 				BOOST_FOREACH(session::ptr session_ptr, model_ptr->get_session_vector())
// 				{
// 					sessions_data_grid->insertRow( 
// 						static_cast<std::vector<std::string> >(boost::assign::list_of("")("Name")) );
// 				}
			}
		};
	}
}


#endif