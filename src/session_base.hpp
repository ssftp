#ifndef __session_base_hpp__
#define __session_base_hpp__

#include <boost/asio.hpp>
#include <boost/function.hpp>
#include <boost/signal.hpp>
#include "connection.hpp"
#include "command.hpp"


namespace ssftp
{
	class session_base
	{
	protected:
		//boost::asio::io_service::strand strand;
		connection conn;

	public:
		session_base(boost::asio::io_service& io_service);

		void on_disconnected(void_handler handler);

		virtual void start()=0;

		ssftp::connection& connection();
		boost::asio::ip::tcp::socket& socket();
	};
}

#endif