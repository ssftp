#ifndef __command_hpp__
#define __command_hpp__

#include <string>


namespace ssftp
{
	struct command
	{
		std::string cmd, args;
		//ctor
		command(std::string cmd="", std::string args="") : cmd(cmd), args(args) {}
	};

	namespace commands
	{
		struct user : command
		{
			user() : command("user") {}
			user(std::string args) : command("user", args) {}
		};
		struct pass : command
		{
			pass() : command("pass") {}
			pass(std::string args) : command("pass", args) {}
		};
	}
}


#endif