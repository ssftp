#ifndef client_hpp_INCLUDED
#define client_hpp_INCLUDED

#include <string>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include "session.hpp"
#include "model.hpp"
#include "../connection.hpp"
#include "../connector.hpp"


namespace ssftp
{
	namespace client
	{
		class client : public model
		{
			boost::asio::io_service io_service;
			boost::asio::io_service::work work;
			session_ptr session_ptr_;

			void user_cmd_handler();

		public:
			client();
			virtual ~client();

			session_ptr session_ptr() { return session_ptr_; }

			bool connect(std::string ip, unsigned short port);

			template<typename handler_type>
			void async_connect(std::string host, unsigned short port, handler_type handler)
			{
				connector connector(session_ptr_->socket());
				connector.async_login(host, port, handler);
			}

			//proxies
			void async_login(std::string username, std::string password, response_handler handler);
		};

		typedef boost::shared_ptr<client> client_ptr;
	}
}

#endif