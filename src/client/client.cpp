#include "client.hpp"
#include "../command.hpp"
#include <boost/thread.hpp>
#include <boost/bind.hpp>


using namespace std;
using namespace boost;
using namespace boost::asio::ip;


ssftp::client::client::client()
	:work(io_service)
{
	thread thread(boost::bind(&boost::asio::io_service::run, &io_service));
}

ssftp::client::client::~client()
{
	io_service.stop();
	io_service.reset();
}

bool ssftp::client::client::connect(string ip, unsigned short port)
{
	session_ptr_.reset(new session(io_service));
	tcp::endpoint ep(address::from_string(ip), port);
	boost::system::error_code error;
	session_ptr_->socket().connect(ep, error);
	if(!error)
		session_ptr_->start();
	return error ? false : true;
}

void ssftp::client::client::async_login(string username, string password, response_handler handler)
{
	session_ptr_->async_login(username, password, handler);
}