#ifndef __view_hpp__
#define __view_hpp__

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/concept_check.hpp>
#include <boost/bind.hpp>
#include "model.hpp"


namespace ssftp
{
	namespace client
	{
		template<typename model_type>
		class view : public boost::noncopyable
		{
			BOOST_CLASS_REQUIRE2(model_type, model, boost, ConvertibleConcept);

			typedef boost::shared_ptr<model_type> model_ptr_type;

		protected:
			model_ptr_type model_ptr;

		public:
			explicit view(model_ptr_type model_ptr)
				:model_ptr(model_ptr)
			{
				model_ptr->on_model_changed(boost::bind(&view::refresh, this));
			}

			virtual ~view()
			{
				model_ptr.reset();
				//m_document.disconnect(m_connection);
			}

			virtual void refresh() const = 0;
		};
	}
}

#endif