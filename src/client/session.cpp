#include "session.hpp"
#include <boost/bind.hpp>

using namespace std;
using namespace boost;


void ssftp::client::session::async_login(string username, string password, response_handler handler)
{
	conn.async_write_command(commands::user(username),
		boost::bind(&session::handle_user_command, this, _1, password, handler));
}

void ssftp::client::session::handle_user_command(response resp, std::string password, response_handler login_handler)
{
	if(resp.code == response_codes::error)
	{
		login_handler(resp);
	}
	else
	{
		conn.async_write_command(commands::pass(password), login_handler);
	}
}


void ssftp::client::session::handle_read_response(response resp)
{
	conn.async_read_response(boost::bind(&session::handle_read_response, this, _1));
}

void ssftp::client::session::start()
{
	conn.async_read_response(boost::bind(&session::handle_read_response, this, _1));
}
