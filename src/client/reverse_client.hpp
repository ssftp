#ifndef __reverse_client_hpp__
#define __reverse_client_hpp__

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include "../connection.hpp"
#include "model.hpp"


namespace ssftp
{
	namespace client
	{	
		//TODO dreckig das hier
		class session;
		typedef boost::shared_ptr<session> session_ptr_type;

		class reverse_client : public model
		{
			typedef boost::shared_ptr<boost::asio::io_service::work> work_ptr_type;

			boost::asio::io_service io_service;
			boost::shared_ptr<boost::asio::ip::tcp::acceptor> acceptor_ptr;
			work_ptr_type work_ptr;
			boost::thread run_thread;

			std::vector<session_ptr_type> sessions;

			void handle_async_accept(const boost::system::error_code& error, session_ptr_type session_ptr);

		public:
			typedef boost::shared_ptr<reverse_client> ptr;

			reverse_client();
			~reverse_client();

			boost::asio::io_service& get_io_service();

			void start_accept_servers(unsigned short port);

			std::vector<session_ptr_type>& get_session_vector()
			{
				return sessions;
			}
		};
	}
}


#endif