#include "reverse_client.hpp"
#include "session.hpp"
#include <boost/bind.hpp>

using namespace boost;
using namespace boost::asio::ip;
using namespace std;


ssftp::client::reverse_client::reverse_client()
	://work_ptr(new asio::io_service::work(io_service))
	run_thread(boost::bind(&boost::asio::io_service::run, &io_service))
{
}

ssftp::client::reverse_client::~reverse_client()
{
	work_ptr.reset();
	run_thread.join();
}

asio::io_service& ssftp::client::reverse_client::get_io_service()
{
	return this->io_service;
}

void ssftp::client::reverse_client::start_accept_servers(unsigned short port)
{
	acceptor_ptr.reset(new tcp::acceptor(io_service, tcp::endpoint(tcp::v4(), port)));
	session_ptr session_ptr(new session(this->get_io_service()));
	acceptor_ptr->async_accept(session_ptr->socket(), bind(&reverse_client::handle_async_accept, this, _1, session_ptr));
}

void ssftp::client::reverse_client::handle_async_accept(const boost::system::error_code& error, session_ptr session_ptr)
{
	if(!error)
	{
		session_ptr->on_disconnected(boost::bind(&reverse_client::refresh_view, this));
		sessions.push_back(session_ptr);
		refresh_view();
	}
	else
	{
		cerr << "client.handle_async_accept: " << error.message() << endl;
	}
}
