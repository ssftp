#ifndef __session_hpp__
#define __session_hpp__

#include <queue>
#include <boost/function.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
//#include "client.hpp"
#include "../connection.hpp"
#include "../session_base.hpp"
#include "../response.hpp"


namespace ssftp
{
	namespace client
	{
		class session : public session_base
		{
			void handle_user_command(response resp, std::string password, response_handler login_handler);

			void handle_read_response(response resp);

		public:
			session(boost::asio::io_service& io_service)
				:session_base(io_service)
			{}

			void async_login(std::string username, std::string password, response_handler handler);

			/**	Fordert Datei&Ordner-Liste vom Server an.
			*/
			std::vector<std::string> list();

			virtual void start();
		};

		typedef boost::shared_ptr<session> session_ptr;
	}
}

#endif