#ifndef __model_hpp__
#define __model_hpp__

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>


namespace ssftp
{
	namespace client
	{
		typedef boost::function<void()> void_handler;

		class model //: private boost::noncopyable
		{
			void_handler model_changed_handler;

		public:
			typedef boost::shared_ptr<model> ptr;

			virtual void refresh_view()
			{
				if(model_changed_handler)
					model_changed_handler();
			}

			virtual void on_model_changed(void_handler handler)
			{
				model_changed_handler = handler;
			}
		};
	}
}

#endif